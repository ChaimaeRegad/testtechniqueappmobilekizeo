/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import RootNavigator from './src/navigation/MainNavigator';
import { NativeBaseProvider } from 'native-base';

const App: () => Node = () => {

  return (
    <NativeBaseProvider><RootNavigator/></NativeBaseProvider>
    
  );
};


export default App;
