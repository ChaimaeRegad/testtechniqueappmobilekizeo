/**
 * @format
 */

import { AppRegistry } from 'react-native';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import App from './App';
import { name as appName } from './app.json';

AppRegistry.registerComponent(appName, () => App );
