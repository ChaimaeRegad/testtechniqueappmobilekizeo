import React, { useEffect } from "react"
import { View, Image, Text } from "react-native"
import { Spinner } from 'native-base';
import { useNavigation } from "@react-navigation/native"
import ApiService from '../services/ApiService'
import StorageService from '../services/StorageService'

export const SplashScreen = function SplashScreen() {


    // Pull in navigation via hook
    const navigation = useNavigation()

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            StorageService.getData('@api_token').then(bearerToken => {
                if (!bearerToken) {
                    navigation.navigate("login");
                } else {
                    ApiService.defaults.headers.common['Authorization'] =
                        'Bearer ' + bearerToken
                    navigation.navigate("sendFile");

                }
            })

        });

        return unsubscribe;

    }, [navigation]);

    return (
        <View style={{ flex: 1, justifyContent: "center", backgroundColor: "#556B2F", alignItems: "center", alignContent: "center" }}>
            <Image
                fadeDuration={0}
                defaultSource={require("../assets/sendFileLogo.png")}
                source={require("../assets/sendFileLogo.png")}
                style={{
                    height: 80,
                    width: 120,
                    alignSelf: "center",
                    resizeMode: "contain",
                    marginBottom: 0
                }}
            />
            <Text style={{
                fontSize: 28,
                fontWeight: 'bold',
                color: "white", marginBottom: 70
            }}>Send File</Text>
            <Spinner color="white" />
        </View>

    )
}



