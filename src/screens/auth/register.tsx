import React, { useState, Component } from 'react'
import { View, StyleSheet, Image } from 'react-native'
import { Container, Stack, Heading, Center, Button, Input, Text } from 'native-base'
import { useNavigation } from "@react-navigation/native"
import Icon from 'react-native-vector-icons/FontAwesome5';
import AuthRepository from '../../repositories/AuthRepository';
import ApiService from '../../services/ApiService'
import StorageService from '../../services/StorageService'

export const RegisterScreen = function RegisterScreen() {

  const navigation = useNavigation();

  const [emailError, setemailError] = useState(false);

  const [email, setemail] = useState('');

  const [name, setname] = useState('');

  const [pswd1, setpswd1] = useState('');

  const [pswd2, setpswd2] = useState('');

  const [passwordError, setpasswordError] = useState(false);

  const [iconPswd1, seticonPswd1] = useState('eye-slash');

  const [hidePswd1, sethidePswd1] = useState(true);

  const [iconPswd2, seticonPswd2] = useState('eye-slash');

  const [hidePswd2, sethidePswd2] = useState(true);

  const register = () => {
    if (pswd1 != pswd2) {

    } else if (pswd1.length < 8) {
      //return error
    } else {
      AuthRepository.postSignUp({
        name: name,
        email: email,
        password: pswd1,
        password_confirmation: pswd2,
      })
        .then((response) => {

          var bearerToken = response.data.token
          ApiService.defaults.headers.common['Authorization'] =
            'Bearer ' + bearerToken
          StorageService.storeData('@api_token', bearerToken)

          navigation.navigate("sendFile")
        }).catch((err) => {
          //error display
          console.log('error', err)

        });
    }
  }

  const changeIcon = (i) => {

    if (i == 1) {
      iconPswd1 == 'eye' ? seticonPswd1('eye-slash') : seticonPswd1('eye')
      sethidePswd1(!hidePswd1)
    } else {

      iconPswd2 == 'eye' ? seticonPswd2('eye-slash') : seticonPswd2('eye')
      sethidePswd2(!hidePswd2)


    }

  }
  const registerScreen = () => {
    return (
      <Stack space={5} style={styles.view} >
        <Image
          fadeDuration={0}
          defaultSource={require("../../assets/sendFileLogo.png")}
          source={require("../../assets/sendFileLogo.png")}
          style={styles.logoAndroid}
        />
        <Heading style={styles.textTitleSignIn} >Register</Heading>
        <Input
          value={name}
          onChangeText={t => { setname(t) }}
          variant="rounded"
          size="lg"
          placeholder="Your Name"
          _light={{
            placeholderTextColor: "blueGray.400",
          }}
          _dark={{
            placeholderTextColor: "blueGray.50",
          }}
        />
        <Input
          value={email}
          onChangeText={t => { setemail(t) }}
          variant="rounded"
          size="lg"
          placeholder="Your Email"
          _light={{
            placeholderTextColor: "blueGray.400",
          }}
          _dark={{
            placeholderTextColor: "blueGray.50",
          }}
        />
        <Input
          value={pswd1}
          onChangeText={t => { setpswd1(t) }}
          variant="rounded"
          size="lg"
          type={hidePswd1 ? "password" : "text"}
          InputRightElement={

            <Icon style={styles.icon} name={iconPswd1} size={22} color="#556B2F" onPress={() => { changeIcon(1) }} />

          }
          placeholder="Passeword"
          _light={{
            placeholderTextColor: "blueGray.400",
          }}
          _dark={{
            placeholderTextColor: "blueGray.50",
          }}
        />
        <Input
          value={pswd2}
          onChangeText={t => { setpswd2(t) }}
          variant="rounded"
          size="lg"
          type={hidePswd2 ? "password" : "text"}
          InputRightElement={

            <Icon style={styles.icon} name={iconPswd2} size={22} color="#556B2F" onPress={() => { changeIcon(2) }} />

          }
          placeholder="Confirme Passeword"
          _light={{
            placeholderTextColor: "blueGray.400",
          }}
          _dark={{
            placeholderTextColor: "blueGray.50",
          }}
        />
        <Button size={'lg'} onPress={() => register()} style={styles.buttonLogin}>
          <Text style={styles.textSignIn}>SignUp</Text>

        </Button>
        <Text style={styles.textHaveAccount}>Already have an account ? <Text style={styles.textSignUp} onPress={() => navigation.navigate('login')}>SignIn</Text></Text>
      </Stack>
    )
  }
  return (
    <View style={styles.container}>
      {registerScreen()}
    </View>

  )

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%"
  },
  view: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    marginBottom: 40,
    marginHorizontal: 15
  },
  textTitleSignIn: {
    color: '#556B2F',
    fontSize: 25,
    fontWeight: 'bold',
  },
  icon: {
    marginHorizontal: 20,
  },
  buttonLogin: {
    alignSelf: "center",
    backgroundColor: "#556B2F",
    width: "100%",
    elevation: 0,
    textTransform: 'lowercase'
  },
  textHaveAccount: {
    borderBottomWidth: 0,
    marginTop: 30,
    color: 'gray',
    fontSize: 15,
    textAlign: "center",
  },
  textSignUp: {
    borderBottomWidth: 0,
    marginTop: 30,
    color: "#556B2F",
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: "center",
  },
  textSignIn: {
    fontSize: 20,
    fontWeight: 'bold',
    color: "white"
  },
  logoAndroid: {
    height: 100,
    width: 150,
    alignSelf: "center",
    resizeMode: "contain",
    marginBottom: -10
  }
});


