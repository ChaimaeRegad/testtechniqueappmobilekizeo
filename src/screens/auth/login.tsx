import React, { useState, Component } from 'react'
import { View, StyleSheet, Image } from 'react-native'
import { Container, Stack, Heading, Center, Button, Input, Text } from 'native-base'
import { useNavigation } from "@react-navigation/native"
import Icon from 'react-native-vector-icons/FontAwesome5';
import AuthRepository from '../../repositories/AuthRepository';
import ApiService from '../../services/ApiService'
import StorageService from '../../services/StorageService'

export const LoginScreen = function LoginScreen() {

  const navigation = useNavigation();

  const [emailError, setemailError] = useState(false);

  const [email, setemail] = useState('');

  const [pswd, setpswd] = useState('');

  const [passwordError, setpasswordError] = useState(false);

  const [iconPswd, seticonPswd] = useState('eye-slash');

  const [hidePswd, sethidePswd] = useState(true);

  const login = () => {

    setemailError(false)
    setpasswordError(false)
    if (email.length < 6 || pswd.length < 6) {
      if (email.length < 6) {
        setemailError(true)
      }
      if (pswd.length < 6) {
        setpasswordError(true)
      }
      return
    }
    AuthRepository.postSignIn({
      email: email,
      password: pswd,
    })
      .then((res) => {
        onLoginOk(res)
      })
      .catch((e) => { onLoginFail(e) });
  }


  const onLoginOk = response => {
    // login
    console.log('ok', response)
    var bearerToken = response.data.token
    ApiService.defaults.headers.common['Authorization'] =
      'Bearer ' + bearerToken
    StorageService.storeData('@api_token', bearerToken)
    navigation.navigate("sendFile")
    // route to main
  }
  const onLoginFail = err => {
    console.log(err)

  }


  const changeIcon = () => {
    iconPswd == 'eye' ? seticonPswd('eye-slash') : seticonPswd('eye')
    sethidePswd(!hidePswd)
  }


  const loginScreen = () => {
    return (
      <Stack space={5} style={styles.view} >
        <Image
          fadeDuration={0}
          defaultSource={require("../../assets/sendFileLogo.png")}
          source={require("../../assets/sendFileLogo.png")}
          style={styles.logoAndroid}
        />
        <Heading style={styles.textTitleSignIn} >SignIn</Heading>
        <Input
          value={email}
          onChangeText={t => { setemail(t) }}
          variant="rounded"
          size="lg"
          placeholder="Your Email"
          _light={{
            placeholderTextColor: "blueGray.400",
          }}
          _dark={{
            placeholderTextColor: "blueGray.50",
          }}
        />
        <Input
          value={pswd}
          onChangeText={t => setpswd(t)}
          variant="rounded"
          size="lg"
          type={hidePswd ? "password" : "text"}
          InputRightElement={

            <Icon style={styles.icon} name={iconPswd} size={22} color="#556B2F" onPress={changeIcon} />

          }
          placeholder="Passeword"
          _light={{
            placeholderTextColor: "blueGray.400",
          }}
          _dark={{
            placeholderTextColor: "blueGray.50",
          }}
        />
        <Button size={'lg'} onPress={() => login()} style={styles.buttonLogin}>
          <Text style={styles.textSignIn}>SignIn</Text>

        </Button>
        <Text style={styles.textHaveAccount}>Don't have an account ? <Text style={styles.textSignUp} onPress={() => navigation.navigate('register')}>SignUp</Text></Text>
      </Stack>
    )
  }

  return (
    <View style={styles.container}>
      {loginScreen()}
    </View>
  )

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%"
  },
  view: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    marginBottom: 40,
    marginHorizontal: 15
  },
  textTitleSignIn: {
    color: '#556B2F',
    fontSize: 25,
    fontWeight: 'bold',
  },
  icon: {
    marginHorizontal: 20,
  },
  buttonLogin: {
    alignSelf: "center",
    backgroundColor: "#556B2F",
    width: "100%",
    elevation: 0,
    textTransform: 'lowercase'
  },
  textHaveAccount: {
    borderBottomWidth: 0,
    marginTop: 30,
    color: 'gray',
    fontSize: 15,
    textAlign: "center",
  },
  textSignUp: {
    borderBottomWidth: 0,
    marginTop: 30,
    color: "#556B2F",
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: "center",
  },
  textSignIn: {
    fontSize: 20,
    fontWeight: 'bold',
    color: "white"
  },
  logoAndroid: {
    height: 100,
    width: 150,
    alignSelf: "center",
    resizeMode: "contain",
    marginBottom: -20
  }
});
