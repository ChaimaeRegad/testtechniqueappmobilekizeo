import React, { useState, Component } from 'react'
import { View, StyleSheet, Image } from 'react-native'
import { Container, Stack, Heading, Center, Button, Input, Text } from 'native-base'
import { useNavigation } from "@react-navigation/native"
import Icon from 'react-native-vector-icons/AntDesign';
import DocumentPicker from 'react-native-document-picker';
import FormRepository from '../repositories/FormRepository';


export const SendFileScreen = function SendFileScreen() {

    const navigation = useNavigation();


    const [title, settitle] = useState(null);

    const [description, setdescription] = useState(null);

    const [recipientEmail, setrecipientEmail] = useState(null);

    const [file, setfile] = useState(null);


    const selectFile = async () => {
        // Opening Document Picker to select one file
        try {
            const res = await DocumentPicker.pick({
                // Provide which type of file you want user to pick
                type: [DocumentPicker.types.allFiles],
            });
            // Printing the log realted to the file
            console.log('res : ' + JSON.stringify(res));
            // Setting the state to show single file attributes
            setfile(res[0]);
        } catch (err) {
            setfile(null);
            // Handling any exception (If any)
            if (DocumentPicker.isCancel(err)) {
                // If user canceled the document selection
                alert('Canceled');
            } else {
                // For Unknown Error
                alert('Unknown Error: ' + JSON.stringify(err));
                throw err;
            }
        }
    };


    const sendFile = () => {
        if (title || description || recipientEmail || file) {
            let formData = new FormData();
            formData.append('title', title);
            formData.append('description', description);
            formData.append('recipient_email', recipientEmail);
            formData.append('file', file);
            FormRepository.sendForm(formData)
                .then((res) => {
                    console.log("file sent", res)
                    settitle(null);
                    setdescription(null);
                    setrecipientEmail(null);
                    setfile(null);
                })
                .catch((err) => {
                    console.log(err, 'rrr')
                    //error display
                });
        }

    }

    const sendFileScreen = () => {
        return (
            <Stack space={5} style={styles.view} >
                <Image
                    fadeDuration={0}
                    defaultSource={require("../assets/sendFileLogo.png")}
                    source={require("../assets/sendFileLogo.png")}
                    style={styles.logoAndroid}
                />
                <Heading style={styles.textTitleSendFile} >Send download file link by email</Heading>
                <Input
                    value={title}
                    onChangeText={t => { settitle(t) }}
                    variant="rounded"
                    size="lg"
                    placeholder="Tile"
                    _light={{
                        placeholderTextColor: "blueGray.400",
                    }}
                    _dark={{
                        placeholderTextColor: "blueGray.50",
                    }}
                />
                <Input
                    value={description}
                    onChangeText={t => { setdescription(t) }}
                    variant="rounded"
                    size="lg"
                    placeholder="Description"
                    _light={{
                        placeholderTextColor: "blueGray.400",
                    }}
                    _dark={{
                        placeholderTextColor: "blueGray.50",
                    }}
                />
                <Input
                    value={recipientEmail}
                    onChangeText={t => { setrecipientEmail(t) }}
                    variant="rounded"
                    size="lg"
                    placeholder="Recipient Email"
                    _light={{
                        placeholderTextColor: "blueGray.400",
                    }}
                    _dark={{
                        placeholderTextColor: "blueGray.50",
                    }}
                />
                {file != null ? (

                    <Center> <Button endIcon={<Icon style={styles.icon} name={"close"} size={22} color="white" onPress={() => { setfile(null) }} />} style={{ backgroundColor: "#556B2F" }}><Text style={{ color: "white" }}>{file.name}</Text>
                    </Button></Center>

                ) : null}
                <Button variant={'outline'} size={'lg'} onPress={() => selectFile()} style={{ borderColor: "#556B2F" }}>
                    <Text style={styles.textSelectFile}>Select File</Text>

                </Button>
                <Button size={'lg'} onPress={() => sendFile()} style={styles.buttonSendFile}>
                    <Text style={styles.textSendFile}>Send File</Text>

                </Button>
            </Stack>
        )
    }
    return (
        <View style={styles.container}>
            {sendFileScreen()}
        </View>

    )


}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%"
    },
    view: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center",
        marginBottom: 40,
        marginHorizontal: 15
    },
    textTitleSendFile: {
        color: '#556B2F',
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    textSelectFile: {
        color: '#556B2F',
        fontWeight: 'bold',
        fontSize: 20,
    },
    icon: {
        marginHorizontal: 0,
    },
    buttonSendFile: {
        alignSelf: "center",
        backgroundColor: "#556B2F",
        width: "100%",
        elevation: 0,
        textTransform: 'lowercase'
    },
    textSendFile: {
        fontSize: 20,
        fontWeight: 'bold',
        color: "white"
    },
    logoAndroid: {
        height: 100,
        width: 150,
        alignSelf: "center",
        resizeMode: "contain",
        marginBottom: -10
    }
});