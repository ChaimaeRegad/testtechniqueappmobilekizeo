import React from "react"
import { createNativeStackNavigator  } from "@react-navigation/native-stack"
import  {LoginScreen, RegisterScreen, SendFileScreen, SplashScreen } from "../screens"
import { NavigationContainer, NavigationContainerRef } from "@react-navigation/native"


export type RootParamList = {
  login: undefined,
  register: undefined,
  sendFile: undefined,
  splash:undefined
}

const Stack = createNativeStackNavigator<RootParamList>()


const RootStack = () => {

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="splash"
    >
      <Stack.Screen name="login" component={LoginScreen} options={{
        headerShown: false,
      }} />
      <Stack.Screen name="register" component={RegisterScreen} options={{
        headerShown: false,
      }} />
      <Stack.Screen name="sendFile" component={SendFileScreen} options={{
        headerShown: false,
      }} />
      <Stack.Screen name="splash" component={SplashScreen} options={{
        headerShown: false,
      }} />
      
    </Stack.Navigator>
  )
}
const RootNavigator = () => {
  return (
    <NavigationContainer>
      <RootStack />
    </NavigationContainer>
  )}
export default RootNavigator
  
