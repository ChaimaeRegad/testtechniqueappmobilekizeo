import axios from '../services/ApiService';

class Form {

    sendForm(data) {
        return axios.post("form", data, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
            .catch(error => {
                return Promise.reject(error);
            });
    }
}

const FormRepository = new Form();
export default FormRepository;
