let config = {
  APP_API_URL: 'https://musing-turing.212-227-9-190.plesk.page/api/',
}

import axios from 'axios';

const instance = axios.create({
  baseURL: config.APP_API_URL,
  timeout: 10000,
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json',
    'Accept': 'application/json',
  }
});

export default instance;
