import AsyncStorage from '@react-native-async-storage/async-storage';

class Storage {

  // Signal server that call ended
  storeData = async (key: any, value: any) => {
    try {
      await AsyncStorage.setItem(key, value)
    } catch (e) {
      // saving error
    }
  }

  deleteData = async (key: any) => {
    try {
      await AsyncStorage.removeItem(key)
    } catch (e) {
      // remove error
    }

    console.log('Done.')
  }

  getData = async (key: any) => {
    try {
      const value = await AsyncStorage.getItem(key)
      return value;
    } catch (e) {
      // error reading value
    }
  }
}

const StorageService = new Storage();
export default StorageService;
